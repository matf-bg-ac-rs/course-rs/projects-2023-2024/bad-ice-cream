QT       += core gui \
            multimedia


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    source/aStar.cpp \
    source/blok.cpp \
    source/clickLabel.cpp \
    source/cudoviste.cpp \
    source/health.cpp \
    source/heroj.cpp \
    source/jakoCudoviste.cpp \
    source/main.cpp \
    source/mainwindow.cpp \
    source/partija.cpp \
    source/playerScore.cpp \
    source/score.cpp \
    source/scoreBoard.cpp \
    source/slaboCudoviste.cpp \
    source/srednjeCudoviste.cpp \
    source/vockica.cpp

HEADERS += \
    include/aStar.h \
    include/blok.h \
    include/clickLabel.h \
    include/cudoviste.h \
    include/graphNode.h \
    include/health.h \
    include/heroj.h \
    include/jakoCudoviste.h \
    include/mainwindow.h \
    include/partija.h \
    include/playerScore.h \
    include/score.h \
    include/scoreBoard.h \
    include/slaboCudoviste.h \
    include/srednjeCudoviste.h \
    include/vockica.h

FORMS += \
    source/mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resorces/res.qrc

DISTFILES += \
    resorces/1.txt \
    resorces/rezultati.csv
