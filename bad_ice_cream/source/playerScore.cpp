#include "include/playerScore.h"

PlayerScore::PlayerScore()
{}

PlayerScore::PlayerScore(const QString& playerName, int score)
    : playerName(playerName), score(score) {}

QString PlayerScore::getPlayerName() const
{
    return playerName;
}

int PlayerScore::getScore() const
{
    return score;
}
