#include "include/scoreBoard.h"

ScoreBoard::ScoreBoard()
{
}

void ScoreBoard::addPlayerScore(const QString& playerName, int score)
{
    PlayerScore playerScore(playerName, score);
    playerScores.append(playerScore);
}

QVector<PlayerScore> ScoreBoard::getPlayerScores() const
{
    return playerScores;
}
