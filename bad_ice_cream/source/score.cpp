#include "include/score.h"
#include <QFont>

Score::Score() {
    score = 0;
    zvukVockice = new QSoundEffect();
    zvukVockice->setSource(QUrl::fromLocalFile(":/sounds/vockica.wav"));
    zvukVockice->setVolume(100.0);
}

Score::~Score()
{
    if (zvukVockice) {
        zvukVockice->stop();  // Možete zaustaviti zvuk pre nego što oslobodite resurs
        delete zvukVockice;
        zvukVockice = nullptr;
    }
}

void Score::increase(int poeni){
    score += poeni;
    zvukVockice->play();

}

int Score::getScore(){
    return score;
}
