#include "include/partija.h"
#include <QGraphicsTextItem>
#include <QElapsedTimer>
#include <QFont>
#include <QBrush>
#include <QTimer>
#include <QLabel>
#include <QGraphicsProxyWidget>
#include <QRandomGenerator>
#include <QImage>
#include <QStackedWidget>
#include "include/heroj.h"
#include "include/cudoviste.h"
#include "include/srednjeCudoviste.h"
#include "include/slaboCudoviste.h"
#include "include/jakoCudoviste.h"
#include <QRandomGenerator>


QElapsedTimer Etimer;
extern int scoreGlobal;

Partija::Partija(int heroType):
    gamePaused(false),
    scene(new QGraphicsScene()),
    health(new Health()),
    score(new Score()),
    scoreBoard(new ScoreBoard()),
    pauseOverlay(nullptr)
{
    inicijalizujTablu();
    postaviInicijalneBlokove(heroType);

    scene->setSceneRect(0,0,1050,840);
    scene->setBackgroundBrush(QBrush(QImage(":/Images/pozadina_isecena.jpg")));

    connect(&timerVockica,SIGNAL(timeout()),this,SLOT(napraviVockicu()));
    connect(&timerHelti,SIGNAL(timeout()),this,SLOT(proveriStanjeHelti()));
    connect(&timerSrednjeCudoviste, &QTimer::timeout, this, [this]() {
        for(int i=0 ; i<2; i++)
            napraviCudoviste(TipCudovista::Srednje);
    });
    connect(&timerJakoCudoviste, &QTimer::timeout, this, [this]() {
        for(int i=0 ; i<2; i++)
            napraviCudoviste(TipCudovista::Jako);
    });
}

void Partija::zapocniPartiju(int heroType)
{
    Etimer.start();

    QString p1, p2;

    switch(heroType){
    case 1: // green
        p1 = ":/Images/heroj3z.png";
        p2 = ":/Images/heroj3zkretanje.png";
        heroj = new Heroj(p1, p2, 6, 8);
        break;
    case 2: // red
        p1 = ":/Images/heroj1_sprite.png";
        p2 = ":/Images/heroj1_kretnja.png";
        heroj = new Heroj(p1, p2, 6, 8);
        break;
    case 3: // blue
        p1 = ":/Images/heroj2z.png";
        p2 = ":/Images/heroj2zkretanje.png";
        heroj = new Heroj(p1, p2, 6, 8);
        break;
    default: //red
        p1 = ":/Images/heroj1_sprite.png";
        p2 = ":/Images/heroj1_kretnja.png";
        heroj = new Heroj(p1, p2, 6, 8);
        break;
    }

    scene->addItem(heroj);

    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(1050,840);

    for(int i=0 ; i<3; i++)
        napraviCudoviste(TipCudovista::Slabo);

    timerJakoCudoviste.start(60 * 1000);
    timerSrednjeCudoviste.start(20 * 1000);
    timerHelti.start(1);
    timerVockica.start(3000);

    emit zapocetaPartija(this);
    emit setupTopBar();
}

void Partija::zavrsiPartiju()
{
    scoreBoard->addPlayerScore(imeIgraca, score->getScore());

    timer.stop();
    timerHelti.stop();
    timerVockica.stop();
    timerJakoCudoviste.stop();
    timerSrednjeCudoviste.stop();

    for (const auto& key : cudovista.keys()) {
        scene->removeItem(cudovista.value(key));
        auto cudoviste=cudovista.value(key);
        cudovista.remove(key);
        delete cudoviste;
    };

    for (const auto& key : vockice.keys()) {
        scene->removeItem(vockice.value(key));
        delete vockice.value(key);
        vockice.remove(key);

    };

    scene->removeItem(heroj);
    scoreGlobal=score->getScore();
    delete heroj;
    delete health;
    delete score;

    emit gameFinished();
}

void Partija::proveriStanjeHelti()
{
    QList<QGraphicsItem *> colliding_items = heroj->collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; ++i){
        if ((typeid(*(colliding_items[i])) == typeid(SlaboCudoviste))
            || (typeid(*(colliding_items[i])) == typeid(SrednjeCudoviste))
            || (typeid(*(colliding_items[i])) == typeid(JakoCudoviste))){
            auto it = cudovista.begin();
            while (it != cudovista.end()) {
                if (it.value() == colliding_items[i]) {
                    it = cudovista.erase(it);
                } else {
                    ++it;
                }
            };
            scene->removeItem(colliding_items[i]);
            delete colliding_items[i];
            if(!heroj->invincible) {
                health->decrease();
                emit healthUpdated(health->getHealth());
                if(health->getHealth()==0){
                    this->zavrsiPartiju();
                    return;
                }
            }
            return;
        }
    }
}

void Partija::inicijalizujTablu()
{
    tabla.resize(12);
    for(int i = 0; i < 12; i++)
        tabla[i].resize(15);
    for(int i = 1; i < 12; i++)
        tabla[i][0] = QPair<int,int>(tabla[i-1][0].first, tabla[i-1][0].second+70);
    for(int i = 0; i < 12; i++)
        for(int j = 1; j < 15; j++)
            tabla[i][j] = QPair<int,int>(tabla[i][j-1].first+70, tabla[i][j-1].second);
}

void Partija::proveriScore()
{
    QList<QGraphicsItem *> colliding_items = heroj->collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; ++i){
        if (typeid(*(colliding_items[i])) == typeid(Vockica)){
            Vockica *vockicaa = dynamic_cast<Vockica*>(colliding_items[i]);
            scene->removeItem(colliding_items[i]);
            vockice.remove(QPair<int,int>(colliding_items[i]->pos().x(), colliding_items[i]->pos().y()));
            score->increase(vockicaa->poeni);
            emit scoreUpdated(score->getScore());
            delete colliding_items[i];
            return;
        }
    }
}

void Partija::postaviInicijalneBlokove(int heroType)
{
    switch(heroType) {
    case 1:
        for(int i = 1; i < tabla.size()-1; i++) {
            std::shared_ptr<Blok> blok = std::make_shared<Blok>();
            blok->setPos(tabla[i][1].first,tabla[i][1].second);
            zauzetaPolja.insert(tabla[i][1], blok);
            scene->addItem(blok.get());
        }

        for(int i = 2; i < tabla[0].size()-2; i++) {
            std::shared_ptr<Blok> blok = std::make_shared<Blok>();
            blok->setPos(tabla[1][i].first,tabla[1][i].second);
            zauzetaPolja.insert(tabla[1][i], blok);
            scene->addItem(blok.get());
        }

        for(int i = 1; i < tabla.size()-1; i++) {
            std::shared_ptr<Blok> blok = std::make_shared<Blok>();
            blok->setPos(tabla[i][tabla[0].size()-2].first,tabla[i][tabla[0].size()-2].second);
            zauzetaPolja.insert(tabla[i][tabla[0].size()-2], blok);
            scene->addItem(blok.get());
        }

        for(int i = 2; i < tabla[0].size()-2; i++) {
            std::shared_ptr<Blok> blok = std::make_shared<Blok>();
            blok->setPos(tabla[tabla.size()-2][i].first,tabla[tabla.size()-2][i].second);
            zauzetaPolja.insert(tabla[tabla.size()-2][i], blok);
            scene->addItem(blok.get());
        }

        for(int i = 3; i < tabla.size()-3; i++) {
            std::shared_ptr<Blok> blok = std::make_shared<Blok>();
            blok->setPos(tabla[i][3].first,tabla[i][3].second);
            zauzetaPolja.insert(tabla[i][3], blok);
            scene->addItem(blok.get());
        }

        for(int i = 4; i < tabla[0].size()-4; i++) {
            std::shared_ptr<Blok> blok = std::make_shared<Blok>();
            blok->setPos(tabla[3][i].first,tabla[3][i].second);
            zauzetaPolja.insert(tabla[3][i], blok);
            scene->addItem(blok.get());
        }

        for(int i = 3; i < tabla.size()-3; i++) {
            std::shared_ptr<Blok> blok = std::make_shared<Blok>();
            blok->setPos(tabla[i][tabla[0].size()-4].first,tabla[i][tabla[0].size()-4].second);
            zauzetaPolja.insert(tabla[i][tabla[0].size()-4], blok);
            scene->addItem(blok.get());
        }

        for(int i = 4; i < tabla[0].size()-4; i++) {
            std::shared_ptr<Blok> blok = std::make_shared<Blok>();
            blok->setPos(tabla[tabla.size()-4][i].first,tabla[tabla.size()-4][i].second);
            zauzetaPolja.insert(tabla[tabla.size()-4][i], blok);
            scene->addItem(blok.get());
        }
        break;
    case 2:
        {
            int maxBlokovaa = (tabla.size() * tabla[0].size()) / 3;
            for (int i = 0; i < maxBlokovaa; ++i) {
                int randomRed, randomKolona;
                do {
                    randomRed = QRandomGenerator::global()->bounded(1, tabla.size() - 1);
                    randomKolona = QRandomGenerator::global()->bounded(2, tabla[0].size() - 2);
                } while (zauzetaPolja.contains(tabla[randomRed][randomKolona]));

                if (randomRed == 6 && randomKolona == 8) {
                    continue;
                }
                std::shared_ptr<Blok> blok = std::make_shared<Blok>();
                blok->setPos(tabla[randomRed][randomKolona].first, tabla[randomRed][randomKolona].second);
                zauzetaPolja.insert(tabla[randomRed][randomKolona], blok);
                scene->addItem(blok.get());
            }
        }
        break;
    case 3:
        for (int i = 1; i < tabla.size() - 1; i += 2) {
            for (int j = 2; j < tabla[0].size() - 2; j++) {
                std::shared_ptr<Blok> blok = std::make_shared<Blok>();
                blok->setPos(tabla[i][j].first, tabla[i][j].second);
                zauzetaPolja.insert(tabla[i][j], blok);
                scene->addItem(blok.get());
            }
        }
        break;
    }
}

void Partija::napraviCudoviste(TipCudovista tip)
{
    QGraphicsItem* parent = nullptr;
    Cudoviste* novoCudoviste = nullptr;

    QString path1;
    QString path2;

    switch (tip) {
    case TipCudovista::Slabo:
        path1 = ":/Images/trol_glavno.png";
        path2 = ":/Images/trol.png";
        novoCudoviste = new SlaboCudoviste(parent, path1, path2,74.0f, 64.0f);
        break;

    case TipCudovista::Srednje:
        path1 = ":/Images/bundeva_glavno.png";
        path2 = ":/Images/bundeva.png";
        novoCudoviste = new SrednjeCudoviste(parent, path1, path2,75.0f, 64.0f);
        timerSrednjeCudoviste.stop();
        break;

    case TipCudovista::Jako:
        path1 = ":/Images/minotaur_glavno.png";
        path2 = ":/Images/minotaur.png";
        novoCudoviste = new JakoCudoviste(parent, path1, path2, 70.0f, 65.0f);
        timerJakoCudoviste.stop();
        break;
    default:
        break;
    }

    if (novoCudoviste) {
        scene->addItem(novoCudoviste);
    }
}

void Partija::napraviVockicu()
{
    QString banana = ":/Images/banane.png";
    QString jabuka = ":/Images/vocke.png";
    QString grozdje = ":/Images/grozdje.png";
    QString chosenType;
    //qint64 elapsedTime = Etimer.elapsed();
    //qreal elapsedTimeSeconds = elapsedTime / 1000.0;
    int poeni = 0;
    int random_value=std::rand()%10;
    if (random_value==1 || random_value==3 || random_value==7 || random_value==8) {
        chosenType = jabuka;
        poeni = 1;
    }
    else if (random_value==2 || random_value == 5 || random_value==9) {
        chosenType = banana;
        poeni = 2;
    }
    else {
        chosenType = grozdje;
        poeni = 3;
    }

    Vockica * vockica = new Vockica(chosenType);
        vockica->poeni = poeni;
    scene->addItem(vockica);
    vockice.insert(QPair<int,int>(vockica->pos().x(),vockica->pos().y()),vockica);
}

void Partija::ukljuciPauzu()
{
    if (!gamePaused) {
        pausedTime = Etimer.elapsed();
        timer.stop();
        timerJakoCudoviste.stop();
        timerSrednjeCudoviste.stop();
        timerVockica.stop();
        for (auto& cudoviste : cudovista) {
            cudoviste->timer.stop();
            cudoviste->timerPokreta.stop();
        }
        gamePaused = true;
        kreirajPauzaOverlay();
        emit pokreniPauzaProzor();
    }
}

void Partija::iskljuciPauzu()
{

    pausedTimes.append(Etimer.elapsed() - pausedTime);
    qint64 pauseSum = std::accumulate(pausedTimes.begin(), pausedTimes.end(), 0);
    qint64 jakoCudoviste_preostaloVreme=60000 - (Etimer.elapsed() - pauseSum);
    qint64 slaboCudoviste_preostaloVreme=20000 - (Etimer.elapsed() - pauseSum);
    if(jakoCudoviste_preostaloVreme>0)
        timerJakoCudoviste.start(jakoCudoviste_preostaloVreme);
    if(slaboCudoviste_preostaloVreme>0)
        timerSrednjeCudoviste.start(slaboCudoviste_preostaloVreme);
    timer.start(3000);
    timerVockica.start(3000);
    for (auto& cudoviste : cudovista) {
        cudoviste->timer.start(450);
        cudoviste->timerPokreta.start(45);
    }
    gamePaused = false;
    ukloniPauzaOverlay();
}

void Partija::kreirajPauzaOverlay()
{
    pauseOverlay = new QGraphicsRectItem(0, 0, scene->width(), scene->height(), nullptr);
    pauseOverlay->setBrush(QBrush(QColor(0, 0, 0, 128)));
    scene->addItem(pauseOverlay);
}

void Partija::ukloniPauzaOverlay()
{
    if (pauseOverlay) {
        scene->removeItem(pauseOverlay);
        delete pauseOverlay;
        pauseOverlay = nullptr;
    }
}
