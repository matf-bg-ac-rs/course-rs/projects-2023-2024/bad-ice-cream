#include "include/partija.h"
#include "include/aStar.h"
#include <QDebug>
#include <QRandomGenerator>

Partija *partija;

AStar::AStar(const QVector<QVector<GraphNode>>& graph, const QPair<int, int>& pocetnaPozicija, const QPair<int, int>& ciljnaPozicija)
    : graph(graph), pocetnaPozicija(pocetnaPozicija), ciljnaPozicija(ciljnaPozicija)
{
}

int AStar::heuristika(const QPair<int, int>& trenutnaPozicija) const
{
    int v_x1 = trenutnaPozicija.first;
    int v_y1 = trenutnaPozicija.second;
    int finish_x1 = ciljnaPozicija.first;
    int finish_y1 = ciljnaPozicija.second;

    return qAbs(v_x1 - finish_x1) + qAbs(v_y1 - finish_y1);
}

QPair<int, int> AStar::pronadjiSledecuPoziciju()
{
    QSet<QPair<int, int>> otvorenaLista;
    QSet<QPair<int, int>> zatvorenaLista;
    QMap<QPair<int, int>, int> g;
    g[pocetnaPozicija] = 0;

    QMap<QPair<int, int>, QPair<int, int>> roditelji;
    roditelji[pocetnaPozicija] = qMakePair(-1, -1);
    otvorenaLista.insert(pocetnaPozicija);

    while (!otvorenaLista.isEmpty()) {
        QPair<int, int> n = qMakePair(-1, -1);
        for (const auto &v : otvorenaLista) {
            if (n == qMakePair(-1, -1) || g[v] + heuristika(v) < g[n] + heuristika(n)) {
                n = v;
            }
        }

        if (n == ciljnaPozicija) {

            QVector<QPair<int, int>> path;
            while (roditelji.contains(n) && n != qMakePair(-1, -1)) {
                path.prepend(n);
                n = roditelji[n];
            }

            if (!path.isEmpty()) {

                return path[1];
            }
        }

        otvorenaLista.remove(n);
        zatvorenaLista.insert(n);

        for (const auto &neighbor : graph[n.first][n.second].neighbors) {
            QPair<int, int> m = neighbor.first->position;
            int weight = neighbor.second;


            if (m.first >= 0 && m.first < graph.size() && m.second >= 0 && m.second < graph[0].size() &&
                !zatvorenaLista.contains(m) && !otvorenaLista.contains(m)) {
                otvorenaLista.insert(m);
                roditelji[m] = n;
                g[m] = g[n] + weight + heuristika(m);
            }
        }
    }

    // ako se zablokirao heroj sa ledom, pozovi astar ponovo ali za blok koji je najblizi heroju i cudovistu

    QPair<int, int> pozicija=randomPozicija();
    return pozicija;
    //    return pocetnaPozicija;
}

QPair<int, int> AStar::randomPozicija()
{
    smer=QRandomGenerator::global()->bounded(4);
    int i = pocetnaPozicija.first;
    int j = pocetnaPozicija.second;
    if (smer == 0){
        if (j > 0 && !partija->zauzetaPolja.contains(partija->tabla[i][j-1])
            && !partija->cudovista.contains(partija->tabla[i][j-1])){
            j--;
        }
    }
    else if (smer == 1){
        if (j < partija->tabla[i].size()-1 && !partija->zauzetaPolja.contains(partija->tabla[i][j+1])&& !partija->cudovista.contains(partija->tabla[i][j+1])){
            j++;
        }
    }
    else if (smer == 2){
        if (i > 0 && !partija->zauzetaPolja.contains(partija->tabla[i-1][j])&& !partija->cudovista.contains(partija->tabla[i-1][j])){
            i--;
        }
    }
    else if (smer == 3){
        if (i < partija->tabla.size()-1 && !partija->zauzetaPolja.contains(partija->tabla[i+1][j])&& !partija->cudovista.contains(partija->tabla[i+1][j])){
            i++;
        }
    }
    return qMakePair(i,j);
}


