#include "include/mainwindow.h"
#include "ui_mainwindow.h"
#include "include/partija.h"
#include "include/scoreBoard.h"
#include <QFile>
#include <QTableWidgetItem>
#include <QLabel>

extern Partija* partija;

QString imeGlobal;
int odigrano=0;
int scoreGlobal=0;
int tipHeroja;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    scoreBoard(new ScoreBoard())
{
    ui->setupUi(this);
    QPixmap bkgnd(":/Images/pozadina.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    scoreBoard = new ScoreBoard();
    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);

    connect(ui->pbOK, &QPushButton::clicked, this, &MainWindow::onOK);
    connect(ui->pbPlayGame, &QPushButton::clicked, this, &MainWindow::onChooseHero);
    connect(ui->pbBackInputName, &QPushButton::clicked, this, &MainWindow::onBackToPage);
    connect(ui->pbBackCH, &QPushButton::clicked, this, &MainWindow::onBackToPage);
    connect(ui->pbExit, &QPushButton::clicked, this, &MainWindow::onExit);
    connect(ui->pbScoreboard, &QPushButton::clicked, this, &MainWindow::onScoreboard);
    connect(ui->pbBack, &QPushButton::clicked, this, &MainWindow::onBackToPage);
    connect(ui->pbMusic, &QPushButton::clicked, this, &MainWindow::onMusic);

    muzika = new QSoundEffect;
    muzika->setSource(QUrl::fromLocalFile(":/sounds/bgmusic1.wav"));
    muzika->setVolume(2.0);
    muzika->play();

    buzzerSound = new QSoundEffect;
    buzzerSound->setSource(QUrl::fromLocalFile(":/sounds/countdown.wav"));
    buzzerSound->setVolume(2.0);

    ui->stackedWidget->setCurrentIndex(1);
    ui->stackedWidget->setGeometry(270, 170, 541, 721);
}

MainWindow::~MainWindow()
{
    delete scoreBoard;
    delete muzika;
    delete buzzerSound;
    delete ui;
}

void MainWindow::onOK()
{
    imeGlobal = ui->leInputName->text();
    if (imeGlobal.length() > 0){
        QString ime_fajla="../bad_ice_cream/resorces/rezultati.csv";
        QFile fajl(ime_fajla);
        int poeni = scoreGlobal;
        QString ime = imeGlobal;
        QString podaci;
        QStringList redovi;
        QStringList red;
        podaci.clear();
        redovi.clear();
        red.clear();
        QTextStream stream(&fajl);
        if(fajl.open(QFile::ReadWrite)) {
            podaci=fajl.readAll();
            redovi=podaci.split("\n");
        }
        else {
            qDebug() << "Nesto nije uredu sa otvaranjem fajla: " << fajl.errorString();
        }
        int poz=0;
        for(int i=0;i<redovi.size()-1;i++) {
            red=redovi.at(i).split(",");
            int brPoena=red.at(1).toInt();
            if(poeni<brPoena){
                poz=poz+1;
            }
        }
        stream.seek(0);
        fajl.resize(0);
        for(int i=0;i<poz && i<10;i++){
            stream<<redovi.at(i)<<"\n";
        }
        if(poz<10){
            stream<<ime<<","<<poeni<<"\n";
            for(int i=poz;i<redovi.size()-1 && i<10;i++){
                stream<<redovi.at(i)<<"\n";
            }
        }
        fajl.close();
    }
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::onChooseHero()
{
    ui->stackedWidget->setGeometry(270, 150, 541, 721);
    ui->stackedWidget->setCurrentIndex(3);
    if(ui->verticalLayoutHero->isEmpty()){
        greenSantaSpriteLabel = new ClickLabel(this);
        QPixmap greenSantaSprite(":/Images/greenHero.png");
        greenSantaSpriteLabel->setPixmap(greenSantaSprite.scaled(45, 71));
        greenSantaSpriteLabel->setAlignment(Qt::AlignCenter);
        ui->verticalLayoutHero->addWidget(greenSantaSpriteLabel);
        //ui->verticalLayoutHero->addSpacing(20);

        redSantaSpriteLabel = new ClickLabel(this);
        QPixmap redSantaSprite(":/Images/redHero.png");
        redSantaSpriteLabel->setPixmap(redSantaSprite.scaled(49, 71));
        redSantaSpriteLabel->setAlignment(Qt::AlignCenter);
        ui->verticalLayoutHero->addWidget(redSantaSpriteLabel);
        //ui->verticalLayoutHero->addSpacing(20);

        blueSantaSpriteLabel = new ClickLabel(this);
        QPixmap blueSantaSprite(":/Images/blueHero.png");
        blueSantaSpriteLabel->setPixmap(blueSantaSprite.scaled(44, 72));
        blueSantaSpriteLabel->setAlignment(Qt::AlignCenter);
        ui->verticalLayoutHero->addWidget(blueSantaSpriteLabel);

        connect(greenSantaSpriteLabel, &ClickLabel::clicked, this, [=](){
            tipHeroja = 1;
            onPlayGame(1);
        });
        connect(redSantaSpriteLabel, &ClickLabel::clicked, this, [=](){
            tipHeroja = 2;
            onPlayGame(2);
        });
        connect(blueSantaSpriteLabel, &ClickLabel::clicked, this, [=](){
            tipHeroja = 3;
            onPlayGame(3);
        });
  }
}

void MainWindow::onPlayGame(int heroType)
{
    if(odigrano > 0)
        delete partija;
    //connect(&countdownTimer, &QTimer::timeout, this, &MainWindow::updateTimeLabel);

    partija = new Partija(heroType);
    connect(partija, &Partija::pokreniPauzaProzor, this, &MainWindow::prikaziPauzaProzor);
    connect(partija, &Partija::healthUpdated, this, &MainWindow::updateHealthLabel);
    connect(partija, &Partija::scoreUpdated, this, &MainWindow::updateScoreLabel);
    connect(partija, &Partija::gameFinished, this, &MainWindow::finishedGame);
    connect(partija, &Partija::zapocetaPartija, this, &MainWindow::zapocetaPartija);
    connect(partija, &Partija::setupTopBar, this, &MainWindow::setupTopBar);
    odigrano++;
    partija->zapocniPartiju(heroType);
    muzika->stop();
}

void MainWindow::onScoreboard()
{
    ui->stackedWidget->setCurrentIndex(2);
    QFile fajl("../bad_ice_cream/resorces/rezultati.csv");

    QString podaci;
    QStringList redovi;
    QStringList red;
    podaci.clear();
    redovi.clear();
    red.clear();

    if(fajl.open(QFile::ReadOnly))
    {
        podaci=fajl.readAll();
        redovi=podaci.split("\n");
        redovi.pop_back();
        fajl.close();
    }

    for(int i=0;i<10 && i<redovi.size();i++)
    {
        red=redovi.at(i).split(",");
        QLabel *nameLabel = new QLabel(red[0].trimmed().append("  ").append(red[1].trimmed()));

        QFont font("Arial Black");
        nameLabel->setFont(font);

        ui->verticalLayoutScoreboard->addWidget(nameLabel);
    }
}

void MainWindow::onBackToPage()
{
    ui->stackedWidget->setGeometry(270, 170, 541, 721);
    QList<QWidget*> widgetList;
    for (int i = 0; i < ui->verticalLayoutScoreboard->count(); ++i) {
        widgetList.append(ui->verticalLayoutScoreboard->itemAt(i)->widget());
    }
    for(auto label : widgetList){
        ui->verticalLayoutScoreboard->removeWidget(label);
    }
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::setupTopBar()
{
    topBarWidget = new QWidget(this);
    topBarLayout = new QHBoxLayout(topBarWidget);
    topBarLayout->setContentsMargins(0,0,5,0);

    healthWidget = new QWidget(this);
    healthLayout = new QHBoxLayout(healthWidget);
    timeWidget = new QWidget(this);
    timeLayout = new QHBoxLayout(timeWidget);
    scoreWidget = new QWidget(this);
    scoreLayout = new QHBoxLayout(scoreWidget);


    for (int i = 0; i < 3; i++) {
        healthSpriteLabel = new QLabel(this);
        QPixmap healthPixmap(":/Images/health.png");
        healthSpriteLabel->setPixmap(healthPixmap.scaled(29, 25));
        healthLayout->addWidget(healthSpriteLabel);
        healthLayout->setAlignment(Qt::AlignLeft);
        healthWidget->setFixedHeight(45);
    }

    topBarLayout->addWidget(healthWidget);

    scoreSpriteLabel = new QLabel(this);
    QPixmap scorePixmap(":/Images/score.png");
    scoreSpriteLabel->setPixmap(scorePixmap.scaled(35, 30));


    scoreLabel = new QLabel("0", this);
    scoreLabel->setStyleSheet("font-weight: bold; color: blue;");
    QFont scoreFont = scoreLabel->font();
    scoreFont.setPointSize(14);
    scoreFont.setBold(true);
    scoreLabel->setFont(scoreFont);

    scoreLayout->addWidget(scoreSpriteLabel);
    scoreLayout->addWidget(scoreLabel);
    scoreLayout->setAlignment(Qt::AlignCenter);
    scoreWidget->setFixedHeight(40);
    scoreWidget->setFixedWidth(150);
    topBarLayout->addWidget(scoreWidget);
    topBarLayout->addSpacing(20);

    timeSpriteLabel = new QLabel(this);
    QPixmap timePixmap(":/Images/clock2.png");
    timeSpriteLabel->setPixmap(timePixmap.scaled(35, 35));

    timeLabel = new QLabel("2m 0s", this);
    timeLabel->setStyleSheet("font-weight: bold; color: blue;");
    QFont timeFont = timeLabel->font();
    timeFont.setPointSize(12);
    timeFont.setBold(true);
    timeLabel->setFont(timeFont);
    remainingTimeInSeconds = 120;
    countdownTimer.start(1000);

    timeLayout->addWidget(timeSpriteLabel);
    timeLayout->addWidget(timeLabel);
    timeLayout->setAlignment(Qt::AlignCenter);
    timeWidget->setFixedHeight(40);
    topBarLayout->addWidget(timeWidget);
    topBarLayout->addSpacing(30);

    pauseSpriteLabel = new ClickLabel(this);
    QPixmap pauseSprite(":/Images/pause.png");
    pauseSpriteLabel->setPixmap(pauseSprite.scaled(30, 30));
    pauseSpriteLabel->setFixedWidth(30);

    topBarLayout->addWidget(pauseSpriteLabel);
    topBarLayout->addSpacing(100);
    connect(pauseSpriteLabel, &ClickLabel::clicked, this, &MainWindow::onTogglePauseResume);

    restartSpriteLabel = new ClickLabel(this);
    QPixmap restartSprite(":/Images/restart.png");
    restartSpriteLabel->setPixmap(restartSprite.scaled(80, 40));
    restartSpriteLabel->setFixedWidth(80); //

    topBarLayout->addWidget(restartSpriteLabel);
    topBarLayout->addSpacing(20);
    connect(restartSpriteLabel, &ClickLabel::clicked, this, &MainWindow::restartGame);

    setTopBarBackground(":/Images/snow.jpg");
    topBarWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    topBarWidget->setFixedHeight(40);

    setMenuWidget(topBarWidget);
}


void MainWindow::setTopBarBackground(const QString &imagePath)
{
    QPalette palette;
    palette.setBrush(QPalette::Window, QBrush(QPixmap(imagePath)));
    topBarWidget->setPalette(palette);
    topBarWidget->setAutoFillBackground(true);
}

void MainWindow::updateHealthLabel()
{
    QWidget* poslednjiWidget = healthLayout->itemAt(healthLayout->count()-1)->widget();
    healthLayout->removeWidget(healthLayout->itemAt(healthLayout->count()-1)->widget());
    delete poslednjiWidget;
}

void MainWindow::updateScoreLabel(int newScore)
{
    scoreLabel->setText(QString::number(newScore));
}

void MainWindow::updateTimeLabel() {
    if (remainingTimeInSeconds > 0) {
        if (partija->gamePaused == false) {
            --remainingTimeInSeconds;
        }
        if (remainingTimeInSeconds == 10) {
            buzzerSound->play();
            disconnect(pauseSpriteLabel, &ClickLabel::clicked, this, &MainWindow::onTogglePauseResume);
        }
        int minutes = remainingTimeInSeconds / 60;
        int seconds = remainingTimeInSeconds % 60;
        timeLabel->setText(QString::number(minutes) + "m " + QString::number(seconds) + "s");
    } else if (remainingTimeInSeconds == 0 && partija) {
        countdownTimer.stop();
        buzzerSound->stop();
        partija->zavrsiPartiju();
        finishedGame();
    }
}

void MainWindow::finishedGame()
{
    topBarWidget->hide();
    ui->stackedWidget->setCurrentIndex(0);
    ui->stackedWidget->setGeometry(270, 170, 541, 721);
    muzika->play();
    buzzerSound->stop();
    disconnect(&countdownTimer, &QTimer::timeout, this, &MainWindow::updateTimeLabel);
}

void MainWindow::onTogglePauseResume()
{
    partija->ukljuciPauzu();
}

void MainWindow::restartGame()
{
    partija->zavrsiPartiju();
    delete partija;
    partija = new Partija(tipHeroja);
    muzika->stop();
    buzzerSound->stop();
    connect(partija, &Partija::pokreniPauzaProzor, this, &MainWindow::prikaziPauzaProzor);
    connect(partija, &Partija::healthUpdated, this, &MainWindow::updateHealthLabel);
    connect(partija, &Partija::scoreUpdated, this, &MainWindow::updateScoreLabel);
    connect(partija, &Partija::gameFinished, this, &MainWindow::finishedGame);
    connect(partija, &Partija::zapocetaPartija, this, &MainWindow::zapocetaPartija);
    connect(partija, &Partija::setupTopBar, this, &MainWindow::setupTopBar);
    partija->zapocniPartiju(tipHeroja);
}

void MainWindow::onMusic()
{
    muzika->setMuted(!muzika->isMuted());
}

void MainWindow::zapocetaPartija(Partija *partija)
{
    connect(&countdownTimer, &QTimer::timeout, this, &MainWindow::updateTimeLabel);
    ui->stackedWidget->addWidget(partija);
    ui->stackedWidget->setCurrentIndex(4);
    ui->stackedWidget->setGeometry(0,0,1050,840);
}

void MainWindow::onExit()
{
    QApplication::quit();
}

void MainWindow::prikaziPauzaProzor()
{
    pauzaProzor = new QDialog(this);
    pauzaProzor->setWindowFlags(Qt::FramelessWindowHint | (pauzaProzor->windowFlags() & ~Qt::WindowCloseButtonHint));
    pauzaProzor->setStyleSheet("QDialog { background-image: url(:/Images/snow.jpg); }");
    pauzaProzor->setWindowTitle("");

    QLabel* labela = new QLabel("Igra je pauzirana.");
    labela->setStyleSheet("color: blue; font-weight: bold; font-size: 18px; text-align: center;"); // Dodano font-size i text-align

    QPushButton* nastaviIgruButton = new QPushButton("Nastavi igru");
    nastaviIgruButton->setStyleSheet("color: blue; font-weight: bold;");

    QPushButton* idiNaPocetniButton = new QPushButton("Idi na početni ekran");
                                      idiNaPocetniButton->setStyleSheet("color: blue; font-weight: bold;");

    connect(nastaviIgruButton, &QPushButton::clicked, [this]() {
        pauzaProzor->accept();
        partija->iskljuciPauzu();
        delete pauzaProzor;
    });

    connect(idiNaPocetniButton, &QPushButton::clicked, [this]() {
        pauzaProzor->accept();
        idiNaPocetniEkran();
        delete pauzaProzor;
    });

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(labela, 0, Qt::AlignCenter);
    layout->addWidget(nastaviIgruButton);
    layout->addWidget(idiNaPocetniButton);

    pauzaProzor->setLayout(layout);
    pauzaProzor->setFixedSize(300, 200);
    pauzaProzor->show();
}


void MainWindow::idiNaPocetniEkran()
{
    disconnect(&countdownTimer, &QTimer::timeout, this, &MainWindow::updateTimeLabel);
    delete topBarWidget;
    delete partija;
    ui->stackedWidget->setGeometry(270, 170, 541, 721);
    muzika->play();
    odigrano=0;
    ui->stackedWidget->setCurrentIndex(1);
}
