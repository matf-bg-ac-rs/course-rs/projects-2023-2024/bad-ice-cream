#include "include/srednjeCudoviste.h"
#include <QRandomGenerator>
#include "include/partija.h"
#include "include/aStar.h"
#include <QDebug>

extern Partija *partija;


SrednjeCudoviste::SrednjeCudoviste(QGraphicsItem *parent, QString path1, QString path2, float sirinaSpritova, float visinaSpritova)
    : Cudoviste(parent, path1, path2, sirinaSpritova, visinaSpritova) {
    t = 0;
    m = 8;

}
SrednjeCudoviste::~SrednjeCudoviste(){}

void SrednjeCudoviste::kretanje()
{

    QPair<int,int> pozicija = qMakePair(pos().x(), pos().y());

    int i = pozicijaCudovista.first;
    int j = pozicijaCudovista.second;
    if(partija->tabla[i][j] != pozicija) {
        if (smer == 0){
            setPixmap(spriteGoreKretnja.at(t%m)->pixmap());
            setPos(pos().x(),pos().y()-5);
        }
        else if (smer == 1){
            setPixmap(spriteDoleKretnja.at(t%m)->pixmap());
            setPos(pos().x(),pos().y()+5);
        }
        else if (smer == 2){
            setPixmap(spriteLevoKretnja.at(t%m)->pixmap());
            setPos(pos().x()-5,pos().y());
        }
        else if (smer == 3){
            setPixmap(spriteDesnoKretnja.at(t%m)->pixmap());
            setPos(pos().x()+5,pos().y());
        }
    }
    else{
        uPokretu = false;
        timerPokreta.stop();
    }
    t++;
}

void SrednjeCudoviste::pomeriSe()
{
    QPair<int,int> novaPozicijaCudovista;
    if(!uPokretu){
        partija->cudovista.remove(partija->tabla[pozicijaCudovista.first][pozicijaCudovista.second]); // brise se na tom mestu cudoviste
        graph=formirajGraf(partija->tabla);

        AStar astar(graph, pozicijaCudovista, partija->heroj->pozicijaNaTabli);
        novaPozicijaCudovista= astar.pronadjiSledecuPoziciju(); // nadjena nova pozicija za cudoviste

        if(novaPozicijaCudovista.first < pozicijaCudovista.first){
            setPixmap(spriteGore->pixmap());
            smer=0;
        }

        else if(novaPozicijaCudovista.first > pozicijaCudovista.first){
            setPixmap(spriteDole->pixmap());
            smer=1;
        }
        else if (novaPozicijaCudovista.second < pozicijaCudovista.second){
            setPixmap(spriteLevo->pixmap());
            smer=2;
        }
        else{

            setPixmap(spriteDesno->pixmap());
            smer=3;
        }

        partija->cudovista.remove(partija->tabla[pozicijaCudovista.first][pozicijaCudovista.second]);
        uPokretu = true;
        partija->cudovista.insert(partija->tabla[novaPozicijaCudovista.first][novaPozicijaCudovista.second], this);
        pozicijaCudovista = novaPozicijaCudovista;
        timerPokreta.start(45);
    }

}

QVector<QVector<GraphNode>> SrednjeCudoviste::formirajGraf(const QVector<QVector<QPair<int, int>>>& tabela) {
    int rows = tabela.size();
    int cols = tabela[0].size();

    QVector<QVector<GraphNode>> graph(rows, QVector<GraphNode>(cols));

    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            graph[i][j].position = qMakePair(i, j);

            if (i > 0 && i < rows && tabela[i - 1][j] != qMakePair(-1, -1)
                && !partija->zauzetaPolja.contains(partija->tabla[i-1][j])
                && !partija->cudovista.contains(partija->tabla[i-1][j])) {
                graph[i][j].neighbors.append(qMakePair(&graph[i - 1][j], 1));
            }

            if (i < rows - 1 && i >= 0 && tabela[i + 1][j] != qMakePair(-1, -1)
                && !partija->zauzetaPolja.contains(partija->tabla[i+1][j])
                && !partija->cudovista.contains(partija->tabla[i+1][j])) {
                graph[i][j].neighbors.append(qMakePair(&graph[i + 1][j], 1));
            }

            if (j > 0 && j < cols && tabela[i][j - 1] != qMakePair(-1, -1)
                && !partija->zauzetaPolja.contains(partija->tabla[i][j-1])
                && !partija->cudovista.contains(partija->tabla[i][j-1])) {
                graph[i][j].neighbors.append(qMakePair(&graph[i][j - 1], 1));
            }

            if (j < cols - 1 && j >= 0 && tabela[i][j + 1] != qMakePair(-1, -1)
                && !partija->zauzetaPolja.contains(partija->tabla[i][j+1])
                && !partija->cudovista.contains(partija->tabla[i][j+1])) {
                graph[i][j].neighbors.append(qMakePair(&graph[i][j + 1], 1));
            }
        }
    }

    return graph;
}




