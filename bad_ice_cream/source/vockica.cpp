#include "include/vockica.h"
#include "include/partija.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QRandomGenerator>
#include <QBrush>
#include <QPixmap>
#include <QString>

extern Partija *partija;

Vockica::Vockica(QString path)
{
    //":/Images/vocke.png"
    QPixmap img = QPixmap(path).copy(0, 32, 16, 16).scaled(25,25, Qt::IgnoreAspectRatio);
    setPixmap(img);

    int i=QRandomGenerator::global()->bounded(12);
    int j=QRandomGenerator::global()->bounded(15);

    while(partija->zauzetaPolja.contains(partija->tabla[i][j])
           || (partija->heroj->pos().x() == partija->tabla[i][j].first && partija->heroj->pos().y() == partija->tabla[i][j].second) || partija->vockice.contains(partija->tabla[i][j])){
        i=QRandomGenerator::global()->bounded(12);
        j=QRandomGenerator::global()->bounded(15);
    }
    setPos(partija->tabla[i][j].first,partija->tabla[i][j].second);
}

Vockica::~Vockica(){
    setPixmap(QPixmap());
}
