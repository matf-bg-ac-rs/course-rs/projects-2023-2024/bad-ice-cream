#include "include/slaboCudoviste.h"
#include <QRandomGenerator>
#include "include/partija.h"
#include <QDebug>
extern Partija * partija;

SlaboCudoviste::SlaboCudoviste(QGraphicsItem *parent, QString path1, QString path2, float sirinaSpritova, float visinaSpritova)
    : Cudoviste(parent, path1, path2, sirinaSpritova, visinaSpritova) {
    t = 0;
    m = 8;

}

SlaboCudoviste::~SlaboCudoviste(){}

void SlaboCudoviste::kretanje()
{

    QPair<int,int> pozicija = qMakePair(pos().x(), pos().y());

    int i = pozicijaCudovista.first;
    int j = pozicijaCudovista.second;
    if(partija->tabla[i][j] != pozicija) {
        if (smer == 0){

            setPixmap(spriteLevoKretnja.at(t%m)->pixmap());
            setPos(pos().x()-5,pos().y());
        }
        else if (smer == 1){
            setPixmap(spriteDesnoKretnja.at(t%m)->pixmap());
            setPos(pos().x()+5,pos().y());

        }
        else if (smer == 2){
            setPixmap(spriteGoreKretnja.at(t%m)->pixmap());
            setPos(pos().x(),pos().y()-5);

        }
        else if (smer == 3){
            setPixmap(spriteDoleKretnja.at(t%m)->pixmap());
            setPos(pos().x(),pos().y()+5);

        }
    }
    else{
        uPokretu = false;
        timerPokreta.stop();
    }
    t++;
}

void SlaboCudoviste::pomeriSe()
{
    if(!uPokretu) {
        int i = pozicijaCudovista.first;
        int j = pozicijaCudovista.second;
        partija->cudovista.remove(partija->tabla[i][j]);

        smer=QRandomGenerator::global()->bounded(4);

        if (smer == 0){
            setPixmap(spriteLevo->pixmap());
            if (j > 0 && !partija->zauzetaPolja.contains(partija->tabla[i][j-1])
                && !partija->cudovista.contains(partija->tabla[i][j-1])){
                j--;
            }
        }
        else if (smer == 1){
            setPixmap(spriteDesno->pixmap());
            if (j < partija->tabla[i].size()-1 && !partija->zauzetaPolja.contains(partija->tabla[i][j+1])&& !partija->cudovista.contains(partija->tabla[i][j+1])){
                j++;
            }
        }
        else if (smer == 2){
            setPixmap(spriteGore->pixmap());
            if (i > 0 && !partija->zauzetaPolja.contains(partija->tabla[i-1][j])&& !partija->cudovista.contains(partija->tabla[i-1][j])){
                i--;
            }
        }
        else if (smer == 3){
            setPixmap(spriteDole->pixmap());
            if (i < partija->tabla.size()-1 && !partija->zauzetaPolja.contains(partija->tabla[i+1][j])&& !partija->cudovista.contains(partija->tabla[i+1][j])){
                i++;
            }
        }
        partija->cudovista.insert(partija->tabla[i][j],this);
        uPokretu = true;
        pozicijaCudovista = qMakePair(i,j);
        timerPokreta.start(45);
    }

}

