#include "include/cudoviste.h"
#include "include/partija.h"
#include <QRandomGenerator>
#include <QString>

extern Partija *partija;

Cudoviste::Cudoviste(QGraphicsItem *parent, QString path1, QString path2, float sirinaSpritova, float visinaSpritova) : QObject(), QGraphicsPixmapItem(parent)
{
    postaviSprajtove(path1, path2, sirinaSpritova, visinaSpritova);
    pozicijaCudovista=postaviPocetnuPoziciju();
    setPos(partija->tabla[pozicijaCudovista.first][pozicijaCudovista.second].first, partija->tabla[pozicijaCudovista.first][pozicijaCudovista.second].second);
    partija->cudovista.insert(partija->tabla[pozicijaCudovista.first][pozicijaCudovista.second],this);

    connect(&timerPokreta, SIGNAL(timeout()), this, SLOT(kretanje()));
    connect(&timer, SIGNAL(timeout()), this, SLOT(pomeriSe()));

    timer.start(850);

}

Cudoviste::~Cudoviste(){
    for(auto &sprite : spriteDesnoKretnja)
        delete sprite;

    for(auto &sprite : spriteLevoKretnja)
        delete sprite;

    for(auto &sprite : spriteGoreKretnja)
        delete sprite;

    for(auto &sprite : spriteDoleKretnja)
        delete sprite;

    delete spriteLevo;
    delete spriteDesno;
    delete spriteDole;
    delete spriteGore;
}

void Cudoviste::postaviSprajtove(QString path1, QString path2, float sirinaSpritova, float visinaSpritova){

    QPixmap spriteSheet = QPixmap(path1);
    QPixmap spriteMovement = QPixmap(path2);
    for (int k = 0; k <= 8; k++)
        spriteLevoKretnja.append(new QGraphicsPixmapItem(spriteMovement.copy(k*visinaSpritova, visinaSpritova, visinaSpritova, visinaSpritova)));

    for (int k = 0; k <= 8; k++)
        spriteDesnoKretnja.append(new QGraphicsPixmapItem(spriteMovement.copy(k*visinaSpritova, 3*visinaSpritova, visinaSpritova, visinaSpritova)));

    for (int k = 0; k <= 8; k++)
        spriteGoreKretnja.append(new QGraphicsPixmapItem(spriteMovement.copy(k*visinaSpritova, 0, visinaSpritova, visinaSpritova)));

    for (int k = 0; k <= 8; k++)
        spriteDoleKretnja.append(new QGraphicsPixmapItem(spriteMovement.copy(k*visinaSpritova, 2*visinaSpritova, visinaSpritova, visinaSpritova)));

    spriteDole = new QGraphicsPixmapItem(spriteSheet.copy(0, 2*visinaSpritova, sirinaSpritova, visinaSpritova));
    spriteLevo = new QGraphicsPixmapItem(spriteSheet.copy(0, 1*visinaSpritova, sirinaSpritova, visinaSpritova));
    spriteDesno = new QGraphicsPixmapItem(spriteSheet.copy(0, 3*visinaSpritova, sirinaSpritova, visinaSpritova));
    spriteGore = new QGraphicsPixmapItem(spriteSheet.copy(0, 0, sirinaSpritova, visinaSpritova));
    setPixmap(spriteGore->pixmap());

}

QPair<int, int> Cudoviste::postaviPocetnuPoziciju()
{
    int i = QRandomGenerator::global()->bounded(12);
    int j = QRandomGenerator::global()->bounded(15);
    uPokretu = false;
    while (partija->zauzetaPolja.contains(partija->tabla[i][j]) ||
           (partija->heroj->pos().x() == partija->tabla[i][j].first && partija->heroj->pos().y() == partija->tabla[i][j].second) ||
           (i >= 4 && i <= partija->tabla.size()-4 && j >= 4 && j <= partija->tabla[0].size()-4)) {
        i = QRandomGenerator::global()->bounded(12);
        j = QRandomGenerator::global()->bounded(15);
    }
    return qMakePair(i,j);

}
