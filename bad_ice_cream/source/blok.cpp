#include "include/blok.h"
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>

Blok::Blok()
{
    QPixmap blok = QPixmap(":/Images/ice_block.png").copy(0, 0, 384, 384).scaled(70, 70, Qt::IgnoreAspectRatio);
    setPixmap(blok);
    setFlag(QGraphicsItem::ItemStacksBehindParent);
}
Blok::~Blok(){
    setPixmap(QPixmap());
}
