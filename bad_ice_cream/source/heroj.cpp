#include <iostream>
#include <QGraphicsScene>
#include <QKeyEvent>
#include <QPair>
#include "include/heroj.h"
#include "include/blok.h"
#include "include/partija.h"
#include <QTimer>
#include <QList>
#include <QPixmap>

extern Partija *partija;

int v = 0;
int s = 8;

Heroj::Heroj(QString path1, QString path2, int posX, int posY):
    pozicijaNaTabli(qMakePair(posX,posY)),
    invincible(false),
    isVisible(false),
    strana(Strana::Dole),
    uPokretu(false)
{   
    setPos(partija->tabla[posX][posY].first, partija->tabla[posX][posY].second);
    postaviSprajtove(path1, path2);

    zvukStvaranjaLeda = new QSoundEffect();
    zvukStvaranjaLeda->setSource(QUrl::fromLocalFile(":/sounds/freeze.wav"));
    zvukStvaranjaLeda->setVolume(100.0);
    zvukUnistavanjaLeda = new QSoundEffect();
    zvukUnistavanjaLeda->setSource(QUrl::fromLocalFile(":/sounds/iceBreak.wav"));
    zvukUnistavanjaLeda->setVolume(0.2);

    connect(&tajmerBesmrtnosti,SIGNAL(timeout()),this,SLOT(besmrtnost()));
    connect(&tajmerBlinkanja,SIGNAL(timeout()),this,SLOT(blinkanje()));
    connect(&timerPokreta, SIGNAL(timeout()), this, SLOT(pokret()));

    setFlag(QGraphicsItem::ItemIsFocusable);
    setFocus();
}

Heroj::~Heroj()
{
    delete spriteLevo;
    delete spriteDesno;
    delete spriteGore;
    delete spriteDole;

    for(auto &key : spriteLevoKretnja)
        delete key;
    for(auto &key : spriteDesnoKretnja)
        delete key;
    for(auto &key : spriteGoreKretnja)
        delete key;
    for(auto &key : spriteDoleKretnja)
        delete key;
}

void Heroj::keyPressEvent(QKeyEvent *event)
{
    if(partija->gamePaused==false){
        if (event->key() == Qt::Key_Left && !uPokretu){
            pomeriSe(Strana::Leva);
        }
        else if (event->key() == Qt::Key_Right && !uPokretu){
            pomeriSe(Strana::Desna);
        }
        else if (event->key() == Qt::Key_Up && !uPokretu){
            pomeriSe(Strana::Gore);
        }
        else if (event->key() == Qt::Key_Down && !uPokretu){
            pomeriSe(Strana::Dole);
        }
        else if (event->key() == Qt::Key_Space){
            stvoriUnistiBlokove();
        }
    }
}

void Heroj::pomeriSe(Strana strana)
{
    int i = pozicijaNaTabli.first;
    int j = pozicijaNaTabli.second;
    switch(strana) {
    case Strana::Leva:
        setPixmap(spriteLevo->pixmap());
        if (j > 0 && !partija->zauzetaPolja.contains(partija->tabla[i][j-1])){
            j--;
        }
        break;
    case Strana::Desna:
        setPixmap(spriteDesno->pixmap());
        if (j < partija->tabla[i].size()-1 && !partija->zauzetaPolja.contains(partija->tabla[i][j+1]))
            j++;
        break;
    case Strana::Gore:
        setPixmap(spriteGore->pixmap());
        if (i > 0 && !partija->zauzetaPolja.contains(partija->tabla[i-1][j]))
            i--;
        break;
    case Strana::Dole:
        setPixmap(spriteDole->pixmap());
        if (i < partija->tabla.size()-1 && !partija->zauzetaPolja.contains(partija->tabla[i+1][j]))
            i++;
        break;
    }

    uPokretu = true;
    this->strana = strana;
    pozicijaNaTabli = qMakePair(i,j);
    timerPokreta.start(30);
}

void Heroj::pokret()
{
    QPair<int, int> pozicija = qMakePair(pos().x(), pos().y());
    int i = pozicijaNaTabli.first;
    int j = pozicijaNaTabli.second;
    if(partija->tabla[i][j] != pozicija) {
        switch(strana) {
        case Strana::Desna:
            setPixmap(spriteDesnoKretnja.at(v%s)->pixmap());
            setPos(pos().x()+10,pos().y());
            break;
        case Strana::Leva:
            setPixmap(spriteLevoKretnja.at(v%s)->pixmap());
            setPos(pos().x()-10,pos().y());
            break;
        case Strana::Gore:
            setPixmap(spriteGoreKretnja.at(v%s)->pixmap());
            setPos(pos().x(),pos().y()-10);
            break;
        case Strana::Dole:
            setPixmap(spriteDoleKretnja.at(v%s)->pixmap());
            setPos(pos().x(),pos().y()+10);
            break;
        }
    } else {
        uPokretu = false;
        timerPokreta.stop();
    }
    v++;
    partija->proveriScore();
}

void Heroj::stvoriUnistiBlokove()
{
    int i = pozicijaNaTabli.first;
    int j = pozicijaNaTabli.second;
    int delay = 110;

    switch(strana) {
    case Strana::Leva:
        if (j > 0 && !partija->zauzetaPolja.contains(partija->tabla[i][j - 1])){
            zvukStvaranjaLeda->play();
            while (j > 0
                   && !partija->zauzetaPolja.contains(partija->tabla[i][j - 1])
                   && !partija->cudovista.contains(partija->tabla[i][j - 1])
                   && !partija->vockice.contains(partija->tabla[i][j - 1])) {
                std::shared_ptr<Blok> blok = std::make_shared<Blok>();
                --j;
                blok->setPos(partija->tabla[i][j].first, partija->tabla[i][j].second);
                QTimer::singleShot(delay * abs(pozicijaNaTabli.second - j), this, [blok]() {
                    partija->scene->addItem(blok.get());
                });
                partija->zauzetaPolja.insert(partija->tabla[i][j], blok);
            }
        } else {
            zvukUnistavanjaLeda->play();
            while (j > 0 && partija->zauzetaPolja.contains(partija->tabla[i][j - 1])) {
                --j;
                auto blok = partija->zauzetaPolja[partija->tabla[i][j]];
                if(partija->zauzetaPolja.contains(partija->tabla[i][j])){
                    QTimer::singleShot(delay * abs(pozicijaNaTabli.second - j), this, [blok]() {
                        if(partija->scene->items().contains(blok.get()))
                            partija->scene->removeItem(blok.get());
                    });
                }
                partija->zauzetaPolja.remove(partija->tabla[i][j]);
            }
            break;
        case Strana::Desna:
            if(j < partija->tabla[i].size()-1 && !partija->zauzetaPolja.contains(partija->tabla[i][j+1])){
                zvukStvaranjaLeda->play();
                while(j < partija->tabla[i].size()-1
                       && !partija->zauzetaPolja.contains(partija->tabla[i][j+1])
                       && !partija->cudovista.contains(partija->tabla[i][j+1])
                       && !partija->vockice.contains(partija->tabla[i][j+1])){
                    std::shared_ptr<Blok> blok = std::make_shared<Blok>();
                    ++j;
                    blok->setPos(partija->tabla[i][j].first, partija->tabla[i][j].second);
                    QTimer::singleShot(delay * abs(pozicijaNaTabli.second - j) , this, [blok]() {
                        partija->scene->addItem(blok.get());
                    });
                    partija->zauzetaPolja.insert(partija->tabla[i][j], blok);
                }
            } else {
                zvukUnistavanjaLeda->play();
                while(j < partija->tabla[i].size()-1 && partija->zauzetaPolja.contains(partija->tabla[i][j+1])){
                    ++j;
                    auto blok = partija->zauzetaPolja[partija->tabla[i][j]];
                    if(partija->zauzetaPolja.contains(partija->tabla[i][j])){
                        QTimer::singleShot(delay * abs(pozicijaNaTabli.second - j) , this, [blok]() {
                            if(partija->scene->items().contains(blok.get()))
                                partija->scene->removeItem(blok.get());
                        });
                    }
                    partija->zauzetaPolja.remove(partija->tabla[i][j]);
                }
            }
            break;
        case Strana::Gore:
            if(i > 0 && !partija->zauzetaPolja.contains(partija->tabla[i-1][j])){
                zvukStvaranjaLeda->play();
                while(i > 0
                       && !partija->zauzetaPolja.contains(partija->tabla[i-1][j])
                       && !partija->cudovista.contains(partija->tabla[i-1][j])
                       && !partija->vockice.contains(partija->tabla[i-1][j])){
                    std::shared_ptr<Blok> blok = std::make_shared<Blok>();
                    --i;
                    blok->setPos(partija->tabla[i][j].first, partija->tabla[i][j].second);
                    QTimer::singleShot(delay * abs(pozicijaNaTabli.first - i) , this, [blok]() {
                        partija->scene->addItem(blok.get());
                    });
                    partija->zauzetaPolja.insert(partija->tabla[i][j], blok);
                }
            } else {
                zvukUnistavanjaLeda->play();
                while(i > 0 && partija->zauzetaPolja.contains(partija->tabla[i-1][j])){
                    --i;
                    auto blok = partija->zauzetaPolja[partija->tabla[i][j]];
                    if(partija->zauzetaPolja.contains(partija->tabla[i][j])){
                        QTimer::singleShot(delay * abs(pozicijaNaTabli.first - i), this, [blok]() {
                            if(partija->scene->items().contains(blok.get()))
                                partija->scene->removeItem(blok.get());
                        });
                    }
                    partija->zauzetaPolja.remove(partija->tabla[i][j]);
                }
            }
            break;
        case Strana::Dole:
            if(i < partija->tabla.size()-1 && !partija->zauzetaPolja.contains(partija->tabla[i+1][j])){
                zvukStvaranjaLeda->play();
                while(i < partija->tabla.size()-1
                       && !partija->zauzetaPolja.contains(partija->tabla[i+1][j])
                       && !partija->cudovista.contains(partija->tabla[i+1][j])
                       && !partija->vockice.contains(partija->tabla[i+1][j])){
                    std::shared_ptr<Blok> blok = std::make_shared<Blok>();
                    ++i;
                    blok->setPos(partija->tabla[i][j].first, partija->tabla[i][j].second);
                    QTimer::singleShot(delay * abs(pozicijaNaTabli.first - i) , this, [blok]() {
                        partija->scene->addItem(blok.get());
                    });
                    partija->zauzetaPolja.insert(partija->tabla[i][j], blok);
                }
            } else {
                zvukUnistavanjaLeda->play();
                while(i < partija->tabla.size()-1 && partija->zauzetaPolja.contains(partija->tabla[i+1][j])){
                    ++i;
                    auto blok = partija->zauzetaPolja[partija->tabla[i][j]];
                    if(partija->zauzetaPolja.contains(partija->tabla[i][j])){
                        QTimer::singleShot(delay * abs(pozicijaNaTabli.first - i), this, [blok]() {
                            if(partija->scene->items().contains(blok.get()))
                                partija->scene->removeItem(blok.get());
                        });
                    }
                    partija->zauzetaPolja.remove(partija->tabla[i][j]);
                }
            }
            break;
        }
    }
}

void Heroj::postaviSprajtove(const QString pathSheet, const QString pathMovement)
{
    float sirinaSpritova = 66;
    float visinaSpritova = 63;
    QPixmap spriteSheet = QPixmap(pathSheet);
    QPixmap spriteMovement = QPixmap(pathMovement);
    for (int k = 0; k <= 8; k++)
        spriteLevoKretnja.append(new QGraphicsPixmapItem(spriteMovement.copy(k*visinaSpritova, visinaSpritova, visinaSpritova, visinaSpritova)));
    for (int k = 0; k <= 8; k++)
        spriteDesnoKretnja.append(new QGraphicsPixmapItem(spriteMovement.copy(k*visinaSpritova, 3*visinaSpritova, visinaSpritova, visinaSpritova)));
    for (int k = 0; k <= 8; k++)
        spriteGoreKretnja.append(new QGraphicsPixmapItem(spriteMovement.copy(k*visinaSpritova, 0, visinaSpritova, visinaSpritova)));
    for (int k = 0; k <= 8; k++)
        spriteDoleKretnja.append(new QGraphicsPixmapItem(spriteMovement.copy(k*visinaSpritova, 2*visinaSpritova, visinaSpritova, visinaSpritova)));

    spriteDole = new QGraphicsPixmapItem(spriteSheet.copy(0, 2*visinaSpritova, sirinaSpritova, visinaSpritova));
    spriteLevo = new QGraphicsPixmapItem(spriteSheet.copy(0, 1*visinaSpritova, sirinaSpritova, visinaSpritova));
    spriteDesno = new QGraphicsPixmapItem(spriteSheet.copy(0, 3*visinaSpritova, sirinaSpritova, visinaSpritova));
    spriteGore = new QGraphicsPixmapItem(spriteSheet.copy(0, 0, sirinaSpritova, visinaSpritova));
    setPixmap(spriteDole->pixmap());
}

void Heroj::besmrtnost()
{
    tajmerBesmrtnosti.stop();
    tajmerBlinkanja.stop();
    invincible = false;
    setVisible(true);
    setFocus();
}

void Heroj::blinkanje()
{
    isVisible = !isVisible;
    setVisible(isVisible);
    setFocus();
}
