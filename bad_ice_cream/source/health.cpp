#include "include/health.h"
#include "include/partija.h"
#include <QFont>

extern Partija *partija;

Health::Health()
{
    health = 3;
    hitSound = new QSoundEffect();
    hitSound->setSource(QUrl::fromLocalFile(":/sounds/hit.wav"));
    hitSound->setVolume(100);
}

void Health::decrease(){
    health--;
    hitSound->play();

    partija->heroj->invincible = true;
    partija->heroj->tajmerBesmrtnosti.start(3000);
    partija->heroj->tajmerBlinkanja.start(100);

}

int Health::getHealth(){
    return health;
}
