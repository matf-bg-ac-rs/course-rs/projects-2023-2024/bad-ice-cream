#ifndef JAKOCUDOVISTE_H
#define JAKOCUDOVISTE_H

#include "include/cudoviste.h"
#include "include/graphNode.h"
#include <QtMultimedia/QSoundEffect>
#include <QtMultimedia>

class JakoCudoviste : public Cudoviste {
private:
    QVector<QVector<GraphNode>> formirajGraf(const QVector<QVector<QPair<int, int>>>& tabela);
    int t;
    int m;
public:
    JakoCudoviste(QGraphicsItem *parent, QString path1, QString path2, float sirinaSpritova, float visinaSpritova);
    virtual ~JakoCudoviste();
    QVector<QVector<GraphNode>> graph;
    void pomeriSe() override;
    void kretanje() override;
    void proveriKoliziju();
    void unistiBlok();
    QSoundEffect *zvukUnistavanjaLeda;
};

#endif // JAKOCUDOVISTE_H
