#ifndef VOCKICA_H
#define VOCKICA_H

#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QString>

class Vockica:public QGraphicsPixmapItem
{
public:
    Vockica(QString path);
    ~Vockica();
    int poeni;
};

#endif // VOCKICA_H
