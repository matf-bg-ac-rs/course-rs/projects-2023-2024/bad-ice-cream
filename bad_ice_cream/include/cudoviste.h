#ifndef CUDOVISTE_H
#define CUDOVISTE_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QString>

class Cudoviste : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Cudoviste(QGraphicsItem *parent = nullptr, QString path1 = nullptr, QString path2 = nullptr, float sirinaSpritova = 0.0f, float visinaSpritova = 0.0f);
    virtual ~Cudoviste();
    QPair<int,int> pozicijaCudovista;
    bool uPokretu;
    QTimer timerPokreta;
    QTimer timer;
    int smer;
    void postaviSprajtove(QString path1, QString path2, float sirinaSpritova, float visinaSpritova);
    QPair<int, int> postaviPocetnuPoziciju();
    QGraphicsPixmapItem *spriteLevo;
    QGraphicsPixmapItem *spriteDesno;
    QGraphicsPixmapItem *spriteGore;
    QGraphicsPixmapItem *spriteDole;

    QList<QGraphicsPixmapItem*> spriteLevoKretnja;
    QList<QGraphicsPixmapItem*> spriteDesnoKretnja;
    QList<QGraphicsPixmapItem*> spriteGoreKretnja;
    QList<QGraphicsPixmapItem*> spriteDoleKretnja;
public slots:
    virtual void pomeriSe() = 0;
    virtual void kretanje() = 0;
};

#endif
