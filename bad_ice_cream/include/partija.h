#ifndef PARTIJA_H
#define PARTIJA_H

#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QPair>
#include <QStackedWidget>
#include "include/heroj.h"
#include "include/blok.h"
#include "include/cudoviste.h"
#include "include/health.h"
#include "include/vockica.h"
#include "include/score.h"
#include "include/scoreBoard.h"


enum TipCudovista {
    Slabo,
    Srednje,
    Jako
};

class Partija : public QGraphicsView
{
    Q_OBJECT
public:
    Partija(int heroType); //

    void zapocniPartiju(int heroType);
    void zavrsiPartiju();
    void proveriScore();
    void ukljuciPauzu();
    void iskljuciPauzu();
    void kreirajPauzaOverlay();
    void ukloniPauzaOverlay();
    bool gamePaused;
    QGraphicsScene *scene;
    Heroj *heroj;
    Health *health;
    Score *score;
    ScoreBoard *scoreBoard;
    QTimer timer;
    QTimer timerJakoCudoviste;
    QTimer timerSrednjeCudoviste;
    QTimer timerVockica;
    QTimer timerHelti;
    QVector<QVector<QPair<int,int>>> tabla;
    QMap<QPair<int,int>,std::shared_ptr<Blok>> zauzetaPolja;
    QMap<QPair<int,int>,Cudoviste *> cudovista;
    QMap<QPair<int,int>,Vockica *> vockice;
signals:
    void pokreniPauzaProzor();
    void healthUpdated(int health);
    void scoreUpdated(int score);
    void timeUpdated(int timeInSeconds);
    void gameFinished();
    void zapocetaPartija(Partija *partija);
    void setupTopBar();
public slots:
    void napraviCudoviste(TipCudovista tip);
    void napraviVockicu();
    void proveriStanjeHelti();
private:
    QString imeIgraca;
    QGraphicsRectItem* pauseOverlay;
    qint64 pausedTime;
    QVector<qint64> pausedTimes;
    void inicijalizujTablu();
    void postaviInicijalneBlokove(int heroType);
};

#endif // PARTIJA_H
