#ifndef HEROJ_H
#define HEROJ_H

#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QMap>
#include <QPair>
#include <QTimer>
#include <QList>
#include <QObject>
#include <QtMultimedia/QSoundEffect>
#include <QtMultimedia>


enum Strana {
    Leva,
    Desna,
    Gore,
    Dole
};

class Heroj: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Heroj(QString path1, QString path2, int posX, int posY);
    ~Heroj();
    void keyPressEvent(QKeyEvent *event);
    QPair<int,int> pozicijaNaTabli;
    bool invincible;
    bool isVisible;
    QTimer tajmerBesmrtnosti;
    QTimer tajmerBlinkanja;
    QTimer timerPokreta;
    Strana strana;
public slots:
    void pokret();
    void besmrtnost();
    void blinkanje();
private:
    void pomeriSe(Strana strana);
    void stvoriUnistiBlokove();
    QGraphicsPixmapItem *spriteLevo;
    QGraphicsPixmapItem *spriteDesno;
    QGraphicsPixmapItem *spriteGore;
    QGraphicsPixmapItem *spriteDole;
    QList<QGraphicsPixmapItem*> spriteLevoKretnja;
    QList<QGraphicsPixmapItem*> spriteDesnoKretnja;
    QList<QGraphicsPixmapItem*> spriteGoreKretnja;
    QList<QGraphicsPixmapItem*> spriteDoleKretnja;
    bool uPokretu;
    void postaviSprajtove(const QString pathSheet, const QString pathMovement);
    QSoundEffect *zvukStvaranjaLeda;
    QSoundEffect *zvukUnistavanjaLeda;
};

#endif // HEROJ_H

