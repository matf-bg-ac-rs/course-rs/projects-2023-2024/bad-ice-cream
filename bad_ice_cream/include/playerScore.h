#ifndef PLAYERSCORE_H
#define PLAYERSCORE_H

#include <QString>

class PlayerScore {
public:
    PlayerScore();
    PlayerScore(const QString& playerName, int score);
    QString getPlayerName() const;
    int getScore() const;

private:
    QString playerName;
    int score;
};

#endif // PLAYERSCORE_H
