#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_mainwindow.h"
#include <QTimer>

#include "include/scoreBoard.h"
#include <QtMultimedia/QSoundEffect>
#include <QtMultimedia>
#include "include/clickLabel.h"
#include "include/partija.h"
#include <QDialog>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void prikaziPauzaProzor();
    void onOK();
    void onPlayGame(int heroType);
    void onChooseHero();
    void onScoreboard();
    void onExit();
    void onBackToPage();
    void updateHealthLabel();
    void updateScoreLabel(int newScore);
    void updateTimeLabel();
    void finishedGame();
    void onTogglePauseResume();
    void restartGame();
    void onMusic();
    void zapocetaPartija(Partija *partija);
    void setupTopBar();
    void idiNaPocetniEkran();
private:
    Ui::MainWindow *ui;
    ScoreBoard* scoreBoard;
    void setTopBarBackground(const QString &imagePath);
    QWidget *topBarWidget;
    QLabel* healthLabel;
    QLabel* scoreLabel;
    QLabel* timeLabel;
    QDialog* pauzaProzor;
    QLabel* timeSpriteLabel;
    QLabel* scoreSpriteLabel;
    QLabel* healthSpriteLabel;
    QWidget* healthWidget;
    QWidget* timeWidget;
    QWidget* scoreWidget;
    QHBoxLayout *topBarLayout;
    QHBoxLayout *healthLayout;
    QHBoxLayout *timeLayout;
    QHBoxLayout *scoreLayout;
    ClickLabel * pauseSpriteLabel;
    ClickLabel * restartSpriteLabel;
    ClickLabel * greenSantaSpriteLabel;
    ClickLabel * redSantaSpriteLabel;
    ClickLabel * blueSantaSpriteLabel;
    QTimer countdownTimer;
    int remainingTimeInSeconds;
    QSoundEffect *muzika;
    QSoundEffect *buzzerSound;
};

#endif // MAINWINDOW_H
