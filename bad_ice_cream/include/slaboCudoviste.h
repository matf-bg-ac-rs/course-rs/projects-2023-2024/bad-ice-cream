#ifndef SLABOCUDOVISTE_H
#define SLABOCUDOVISTE_H

#include "include/cudoviste.h"

class SlaboCudoviste : public Cudoviste
{
    Q_OBJECT
public:
    SlaboCudoviste(QGraphicsItem *parent, QString path1, QString path2, float sirinaSpritova, float visinaSpritova);
    virtual ~SlaboCudoviste();
    void pomeriSe() override;
    void kretanje() override;

private:
    int t;
    int m;
};

#endif // SLABOCUDOVISTE_H
