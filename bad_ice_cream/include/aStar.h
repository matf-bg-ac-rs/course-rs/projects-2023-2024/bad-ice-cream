#ifndef ASTAR_H
#define ASTAR_H

#include <QVector>
#include <QPoint>
#include "include/graphNode.h"

class AStar
{
public:
    AStar(const QVector<QVector<GraphNode>>& graph, const QPair<int, int>& pocetnaPozicija, const QPair<int, int>& ciljnaPozicija);
    QPair<int, int> pronadjiSledecuPoziciju();

private:
    QVector<QVector<GraphNode>> graph;
    QPair<int, int> pocetnaPozicija;
    QPair<int, int> ciljnaPozicija;
    int heuristika(const QPair<int, int>& trenutnaPozicija) const;
    int smer;
    QPair<int, int> randomPozicija();

};

#endif // ASTAR_H
