#ifndef SCORE_H
#define SCORE_H

#include <QGraphicsTextItem>
#include <QtMultimedia/QSoundEffect>
#include <QtMultimedia>
#include <QMap>
#include <QString>

class Score {
public:
    Score();
    Score(int x0);
    ~Score();
    void increase(int poeni);
    int getScore();
private:
    int score;
    QMap<QString, int> m_rezultat;
    QSoundEffect *zvukVockice;
};

#endif // SCORE_H
