#ifndef HEALTH_H
#define HEALTH_H

#include <QGraphicsTextItem>
#include <QtMultimedia/QSoundEffect>
#include <QtMultimedia>
#include "ui_mainwindow.h"

class Health {
public:
    Health();
    void decrease();
    int getHealth();
private:
    int health;
    QSoundEffect *hitSound;
};


#endif // HEALTH_H
