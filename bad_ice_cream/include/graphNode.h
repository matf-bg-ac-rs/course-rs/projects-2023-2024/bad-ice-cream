#ifndef GRAPHNODE_H
#define GRAPHNODE_H

#include <QPair>
#include <QVector>

struct GraphNode {
    QPair<int, int> position;
    QVector<QPair<GraphNode*, int>> neighbors;
};

#endif // GRAPHNODE_H
