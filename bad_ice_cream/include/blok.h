#ifndef BLOK_H
#define BLOK_H

#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>

class Blok: public QGraphicsPixmapItem
{
public:
    Blok();
    ~Blok();
};

#endif // BLOK_H
