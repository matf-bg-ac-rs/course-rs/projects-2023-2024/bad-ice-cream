#ifndef SREDNJE_CUDOVISTE_H
#define SREDNJE_CUDOVISTE_H

#include "include/cudoviste.h"
#include "include/graphNode.h"

class SrednjeCudoviste : public Cudoviste {
private:
    QVector<QVector<GraphNode>> formirajGraf(const QVector<QVector<QPair<int, int>>>& tabela);

    int t;
    int m;

public:
    SrednjeCudoviste(QGraphicsItem *parent, QString path1, QString path2, float sirinaSpritova, float visinaSpritova);
    virtual ~SrednjeCudoviste();
    QVector<QVector<GraphNode>> graph;
    void pomeriSe() override;
    void kretanje() override;

    void postaviKretnju();
    void postaviOkretanje();
};

#endif // SREDNJE_CUDOVISTE_H

