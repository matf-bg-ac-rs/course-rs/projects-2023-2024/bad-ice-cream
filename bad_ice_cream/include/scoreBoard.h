#ifndef SCOREBOARD_H
#define SCOREBOARD_H

#include <QWidget>
#include <QLabel>

#include <QVector>
#include "playerScore.h"

class ScoreBoard {
public:
    ScoreBoard();
    void addPlayerScore(const QString& playerName, int score);
    QVector<PlayerScore> getPlayerScores() const;


private:
    QVector<PlayerScore> playerScores;
};

#endif // SCOREBOARD_H

