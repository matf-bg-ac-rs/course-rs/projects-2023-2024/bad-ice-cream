# Bad Santa

Partija se započinje automatski klikom na dugme zapocni igru i odabira heroja. Po mapi se kreće igrač(deda mraz), čiji je cilj da skupi sto vise voćki na terenu, dok u isto vreme izbegava čudovista. Nakon odigrane partije, aplikacija trazi od igrača da unese ime, vraća ga na glavni meni i ažurira ostvareni rezultat.

DEMO SNIMAK:
 - <a href="https://www.youtube.com/watch?v=Kdvd2St-sUg&t=288s"> youtube snimak</a>
 
Članovi:
 - <a href="https://gitlab.com/viktorski5">Viktor Nikić 81/2019</a>
 - <a href="https://gitlab.com/vukvuk21">Vuk Stefanović 66/2019</a>
 - <a href="https://gitlab.com/VojkanUE">Vojkan Panić 138/2019</a>
 - <a href="https://gitlab.com/dunjamijacic">Dunja Mijačić 74/2019</a>
 - <a href="https://gitlab.com/igorz999">Igor Zolotarev 228/2019</a>
 - <a href="https://gitlab.com/lazardackovic">Lazar Dačković 224/2019</a>
